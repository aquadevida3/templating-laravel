<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class indexController extends Controller
{
    public function index()
    {
        return view('home');
    }
    public function datatable()
    {
        return view('table.data-tables');
    }
}
